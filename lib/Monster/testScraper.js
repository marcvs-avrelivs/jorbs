#!/usr/bin/env node
const {MonsterScraper} = require("./Scraper.js");
(async () => {
  const scrape = new MonsterScraper();
  const opts = {
    query: 'Management',
    location: 'Carlisle, PA',
  };
  const jobs = await scrape.scrape(opts);
  console.log(JSON.stringify(jobs,null,2));
})();
