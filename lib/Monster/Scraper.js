const axios = require('axios');
const cheerio = require('cheerio');
const SummarizerManager = require("node-summarizer").SummarizerManager;


class MonsterScraper {
  urify = (fragment) => encodeURIComponent(
    fragment.replace(/ /g,'-')
  ).replace(/%/g,'__');
  url = (q,l) => {
    const query = this.urify(q);
    const loc = this.urify(l);
    return `https://www.monster.com/jobs/search/?q=${query}&where=${loc}`;
  };
  getPage = async (url) => {
    return await axios.get(url);
  };
  loadPage = async (url) => {
    const {data} = await this.getPage(url);
    return cheerio.load(data);
  };
  loadString = (string) => cheerio.load(string);
  str = "";
  scrape = async (opts) => {
    const {query, location} = opts;
    if(!query) {
      throw new Error('No query given');
    } else if(!location) {
      throw new Error('No location given');
    }
    const url = this.url(query,location);
    this.string = await this.getPage(url);
    const $ = await this.loadPage(url);
    const res = $("#SearchResults .card-content");
    const jobs = [];
    res.each((i, e) => {
      const job = {};
      const tElem = $('.title a',e)
      const company = $('.company .name', e).text().trim();
      const location = $('.location .name', e).text().trim();
      const postDate = $(".meta time", e);
      const url = tElem.attr('href');
      if(typeof url === 'undefined') {return;} //no scrubs
      const title = tElem.text().trim();
      job.url = url;
      job.title = title;
      job.location = location;
      job.company = company;
      job.postDate = postDate.text();
      //job.timestamp = postDate.attr('datetime');


      jobs.push(job);
    })
    const prm = [];
    for(const idx in jobs) {
      prm.push(new Promise((resolve, reject) => {
        const url = encodeURI(jobs[idx].url);
        this.loadPage(url).then(async ($$) => {
          const description = $$('.details-content').text();
          const Summarizer = new SummarizerManager(description, 2);
          jobs[idx].freqrank = Summarizer.getSummaryByFrequency().summary.replace(/\s+/g, " ");
          /*
          if(jobs[idx].freqrank.length > 150) {
            jobs[idx].freqrank = jobs[idx].freqrank.slice(0,150).trim()+"...";
          }
          */
          jobs[idx].summary = Summarizer.getSummaryByRank().then((sum) => {
            jobs[idx].summary = sum.summary.replace(/\s+/g, " ");
            /*
            if(jobs[idx].summary.length > 150) {
              jobs[idx].summary = jobs[idx].summary.slice(0,150).trim()+"...";
            }
            */
            resolve(jobs[idx].summary);
          })
        })
      }));
    }
    await Promise.all(prm);
    return jobs;
    }
  constructor() {
    // whooooosh
  }


}

module.exports.MonsterScraper = MonsterScraper;
