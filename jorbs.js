#!/usr/bin/env node
const rl = require("readline-sync");
const indeed = require('indeed-scraper');
const wrap = require('word-wrap');
const chrono = require('chrono-node');
const {MonsterScraper} = require('./lib/Monster/Scraper.js');

const args = process.argv.slice(2);

let q;
if(args.length) {
  q = args.join(" ");
} else {
  q = rl.question("Enter keyword for search: ");
}

const mOpts = {
  query: q,
  location: 'Carlisle, PA',
};
const Scraper = new MonsterScraper();
Scraper.scrape(mOpts).then((MonstRes) => {
  const queryOptions = {
    host: 'www.indeed.com',
    query: q,
    city: 'Carlisle, PA',
    radius: '50',
    sort: 'date',
    limit: 20,
  };

  indeed.query(queryOptions).then(
    res => {  
      const jorbs = res.concat(MonstRes);
      console.log(JSON.stringify(
      jorbs.map(
        e => {
          e.summary = e.summary.replace(/Job Summary:/i, '');
          if(e.summary.replace(/\.\.\.$/,"").length >= 150) {
            e.summary = e.summary.slice(0,150).trim()+"...";
          }
            e.timestamp =chrono.parseDate(
                e.postDate.replace(/\+/,"").
                  replace(/Just posted/i,"now")
                );
          return e;
        }
        ).
        sort((a,b) => {
          const dateA = new Date(a.timestamp);
          const dateB = new Date(b.timestamp);
          return dateB.getTime() - dateA.getTime();
        })
      ,null
      ,2
    )) //res.forEach( job => dumpJob(job) )
  });
  /*
  function sep(char,length){
    length = length || 80;
    char = char || "-";
    let iter = 0;
    while((process.stdout.write(char)) && ++iter < length );
    console.log('');
  }

  function dumpJob(job) {
    sep();
    const {company, title, location, summary, salary} = job;
    console.log(`Company: ${company}`);
    console.log(`Title: ${title}`);
    console.log(`Location: ${location}`);
    console.log(wrap(`Summary: ${summary}`,{
      width: 80,
      indent:'',
    }));
    if(salary !== "") {
      console.log(`Salary: ${salary}`);
    }
  }
  */
});
